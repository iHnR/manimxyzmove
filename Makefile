all:
	@python3 -m build

upload:
	@make all
	@python3 -m twine upload dist/*
