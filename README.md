# XYZPath
## Basic description
This package contains the `MoveAlongXYZPath` object. It is a simple utility that allows for moving across a path defined by an array of vectors (optionally with timestamps).  

## Motivation
I had a few simulations that returned data in this form. It seemed like an obvious use case but all high level classes in manim required really backwards ways of implementing (afaik, maybe I just missed something).

## Future works
- I might add some interpolation options to the object to allow for smooth movement with less samples. 

## Disclaimer
This is the first project I've packaged. It's mostly for personal learning.
